<?php

namespace Project\Vote;

use cUser,
    CIBlockElement,
    SplFileObject,
    Bitrix\Main\Application,
    Bitrix\Main\Loader,
    Bitrix\Main\Entity,
    Project\Vote\Model\VoteTable,
    Project\Tools\Utility;

class Vote {

    public static function status($sectionId, $elementId) {
        $res = VoteTable::getList(array(
                    'select' => array('ID'),
                    'filter' => array(
                        'SECTION_ID' => $sectionId,
                        'ELEMENT_ID' => $elementId,
                        'USER_ID' => cUser::GetID(),
                    )
        ));
        return (bool) $res->Fetch();
    }

    public static function add($sectionId, $elementId, $userId) {
        Application::getConnection()->startTransaction();
        try {
            $res = VoteTable::getList(array(
                        'select' => array('ID'),
                        'filter' => array(
                            'SECTION_ID' => $sectionId,
                            'ELEMENT_ID' => $elementId,
                            'USER_ID' => cUser::GetID(),
                        )
            ));
            if (!$arVote = $res->Fetch()) {
                VoteTable::add(array(
                    'SECTION_ID' => $sectionId,
                    'ELEMENT_ID' => $elementId,
                    'USER_ID' => cUser::GetID(),
                    'VOTE' => $userId,
                ));
            }
        } catch (Exception $e) {
            Application::getConnection()->rollbackTransaction();
            return Utility::ThrowException(Loc::getMessage($this->message . '_' . $message, array(
                                'ERROR' => $e->getMessage()
            )));
        }
        Application::getConnection()->commitTransaction();
        return false;
    }

    static public function export($file, $sectionId) {
        $file = new SplFileObject($file, 'w');
        $file->setFlags(SplFileObject::READ_CSV);

        $res = CIBlockElement::GetList(array('SORT' => 'ASC'), array('ACTIVE' => 'Y', 'IBLOCK_ID' => 110, 'SECTION_ID' => $sectionId), false, false, array('ID', 'NAME'));
        $arResult = array();
        $arData = array('Номинация');
        while ($arItem = $res->Fetch()) {
            $arData[] = $arItem['NAME'];
            $arResult[$arItem['ID']] = $arItem['NAME'];
        }
        $file->fputcsv(Utility\Content::toWin1251($arData), ';');

        $arUsers = User::getList(array('ACTIVE' => 'Y'));
        $resVote = VoteTable::getList(array(
                    'select' => array(
                        'ELEMENT_ID',
                        'VOTE',
                        'VOTE_SUM'
                    ),
                    'filter' => array(
                        'SECTION_ID' => $sectionId,
                        'ELEMENT_ID' => array_keys($arResult),
                    ),
                    'runtime' => array(
                        new Entity\ExpressionField('VOTE_SUM', 'COUNT(USER_ID)')
                    )
        ));
        while ($arItem = $resVote->Fetch()) {
            $arUsers[$arItem['VOTE']]['VOTE'][$arItem['ELEMENT_ID']] = $arItem['VOTE_SUM'];
        }

        foreach ($arUsers as $key => $arUser) {
            $arData = array($arUser['NAME']);
            foreach ($arResult as $key => $value) {
                $arData[] = $arUser['VOTE'][$key] ?: '';
            }
            $file->fputcsv(Utility\Content::toWin1251($arData), ';');
        }
    }

}
