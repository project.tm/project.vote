<?php

namespace Project\Vote\Model;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class VoteTable extends DataManager {

    public static function getTableName() {
        return 'd_project_vote';
    }

    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\IntegerField('SECTION_ID'),
            new Main\Entity\IntegerField('ELEMENT_ID'),
            new Main\Entity\IntegerField('USER_ID'),
            new Main\Entity\IntegerField('VOTE'),
        );
    }

}
