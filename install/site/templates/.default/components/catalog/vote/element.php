<?

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;

Loader::includeModule('project.vote');

$sectionUrl = str_replace('#SECTION_CODE#', $arResult["VARIABLES"]["SECTION_CODE"], $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"]);
if (Project\Vote\Start::isStart($arParams['IBLOCK_ID'], $arResult['VARIABLES'])) {
    LocalRedirect($sectionUrl);
}

$APPLICATION->IncludeComponent('project:ajax.wrapper', 'vote.user', array(
    'PARAM' => array(
        'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
        'IBLOCK_ID' => $arParams['IBLOCK_ID'],
        'SECTION_URL' => $sectionUrl,
        'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
        'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
        'ELEMENT_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
        'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
        'FIELD_CODE' => $arParams['DETAIL_FIELD_CODE'],
        'PROPERTY_CODE' => $arParams['DETAIL_PROPERTY_CODE'],
)));
