<?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetAdditionalCSS('/local/modules/project.vote/tools/css/vote.css');
$APPLICATION->SetAdditionalCSS('/local/modules/project.vote/tools/fonts/stylesheet.css');
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetPageProperty("title", 'Голосование');
?>
<?

$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
    "AREA_FILE_SHOW" => "file",
    "PATH" => SITE_DIR . "vote/include/start.php",
));
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>