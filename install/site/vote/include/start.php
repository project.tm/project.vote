<div class="ny_body_start ny_start">
    <div class="border_block">
        <div class="ny_start__content">
            <div class="ny_start__title">Приветствуем Вас, коллеги!</div>
            <div class="ny_start__text">Вот и пришел праздник нашего детства. Поздравляем всех, друзья! Пусть каждый день, каждый миг приносит радость и открывает прелести бытия. Мы всем желаем, чтобы жизнь сверкала как фейерверк, который никогда не иссякает. Этот яркий символ всегда напомнит, какой яркой может быть жизнь. С праздником!
            </div>
            <div class="center">
                <a class="ny_start__next_step red_btn" href="<?= $APPLICATION->GetCurPage() ?>start/">Начнем голосование!</a>
            </div>
        </div>
    </div>
</div>








